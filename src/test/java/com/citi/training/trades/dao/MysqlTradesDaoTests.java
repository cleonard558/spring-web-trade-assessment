package com.citi.training.trades.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.model.Trades;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradesDaoTests {

	@Autowired
	MysqlTradesDao mysqlTradesDao;

	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlTradesDao.create(new Trades(-1, "SONY", 10.0, 100));

		assertEquals(mysqlTradesDao.findAll().size(), 1);
	}

	@Test
	@Transactional
	public void test_deleteTrades() {
		Trades test = mysqlTradesDao.create(new Trades(-1,"APPEL", 1000.0,10));
		Trades test2 = mysqlTradesDao.create(new Trades(-1,"CITI",1000.0,10));
		
		mysqlTradesDao.deleteById(test2.getId());
		assertEquals(mysqlTradesDao.findAll().size(),1);
		assertThat(test).isEqualToComparingFieldByField(mysqlTradesDao.findById(test.getId()));
	}
}
