package com.citi.training.trades.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trades.exceptions.TradesNotFoundException;
import com.citi.training.trades.model.Trades;

public class RamTradeDaoTests {

    private static Logger LOG = LoggerFactory.getLogger(RamTradeDaoTests.class);

    private int testId = 99;
    private String testStock = "GOOGL";
    private double testPrice = 123.99;
    private int testVolume = 1000;

    @Test
    public void test_storeAndRetrieveTrades() {
        Trades testTrades = new Trades(-1, testStock, testPrice,testVolume);
        RamTradeDao testRamRepo = new RamTradeDao();

        testTrades = testRamRepo.create(testTrades);
 
        assertTrue(testRamRepo.findById(testTrades.getId()).equals(testTrades));

    }

    @Test
    public void test_storeAndRetrieveAllTrades() {
    	Trades[] testTradesArray = new Trades[20];
    	RamTradeDao testRamRepo = new RamTradeDao();

        for(int i=0; i<testTradesArray.length; ++i) {
        	testTradesArray[i] = new Trades(-1, testStock, testPrice,testVolume);

        	testRamRepo.create(testTradesArray[i]);
        }

        List<Trades> retrievedTrades = testRamRepo.findAll();
        LOG.info("Received [" + retrievedTrades.size() +
                 "] Trades from RAM repo");

        for(Trades thisTrade: testTradesArray) {
            assertTrue(retrievedTrades.contains(thisTrade));
        }
        LOG.info("Matched [" + testTradesArray.length + "] Trades");
    }

    @Test (expected = TradesNotFoundException.class)
    public void test_deleteTrades() {
    	Trades[] testTradesArray = new Trades[20];
    	RamTradeDao testRamRepo = new RamTradeDao();

        for(int i=0; i<testTradesArray.length; ++i) {
        	testTradesArray[i] = new Trades(testId + i, testStock, testPrice,testVolume);

        	testRamRepo.create(testTradesArray[i]);
        }
        Trades removedTrade = testTradesArray[5];
        testRamRepo.deleteById(removedTrade.getId());
    	LOG.info("Removed item from Trade array");
    	testRamRepo.findById(removedTrade.getId());
    }
}

