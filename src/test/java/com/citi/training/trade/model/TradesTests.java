package com.citi.training.trade.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.citi.training.trades.model.Trades;

public class TradesTests {

    private int testId = 23;
    private String testName = "AAPL";
    private double testPrice = 12.34;
    private int testVolume = 10;

    @Test
    public void test_Trades_constructor() {
        Trades testTrades = new Trades(testId,testName,testPrice,testVolume);;

        assertEquals(testId, testTrades.getId());
        assertEquals(testName, testTrades.getStock());
        assertEquals(testPrice, testTrades.getPrice(), 0.0001);
        assertEquals(testVolume, testTrades.getVolume());
    }

    @Test
    public void test_Trades_toString() {
        String testString = new Trades(testId,testName,testPrice,testVolume).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains((new Double(testPrice).toString())));
        assertTrue(testString.contains((new Integer(testVolume)).toString()));
    }
}
