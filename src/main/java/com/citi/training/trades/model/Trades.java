package com.citi.training.trades.model;

public class Trades {

	private String stock;
	private int id;
	private double price;
	private int volume;
	
	public Trades() {}
	
	public Trades(int stockId, String stockName, double stockPrice, int stockVolume){
		this.id = stockId;
		this.stock = stockName;
		this.price = stockPrice;
		this.volume = stockVolume;
	}
	
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
}
