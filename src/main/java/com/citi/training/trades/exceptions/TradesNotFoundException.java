package com.citi.training.trades.exceptions;

@SuppressWarnings("serial")
public class TradesNotFoundException extends RuntimeException {

    public TradesNotFoundException(String msg) {
        super(msg);
    }
}
