package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trades;

public interface TradesDao {

	List<Trades> findAll();

	Trades findById(int id);

	Trades create(Trades trades);

	void deleteById(int id);
}
